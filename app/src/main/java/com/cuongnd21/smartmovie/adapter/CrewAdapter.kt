package com.cuongnd21.smartmovie.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cuongnd21.smartmovie.R
import com.cuongnd21.smartmovie.model.Cast
import com.cuongnd21.smartmovie.ultis.Constains
import com.cuongnd21.smartmovie.ultis.CrewDiffUltis

class CrewAdapter : RecyclerView.Adapter<CrewAdapter.ViewHolder>() {
    private val casts = arrayListOf<Cast>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cast, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(casts[position])
    }

    override fun getItemCount(): Int = casts.size

    fun addData(cast: List<Cast>) {
        try {
            val diffUtils =
                DiffUtil.calculateDiff(CrewDiffUltis(oldData = casts, newData = cast))
            diffUtils.dispatchUpdatesTo(this)
            casts.clear()
            casts.addAll(cast)
        } catch (ex: Exception) {
            casts.clear()
            casts.addAll(cast)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val txtNameCaster: TextView = itemView.findViewById(R.id.txtNameCaster)
        private val imgCaster: ImageView = itemView.findViewById(R.id.imgCaster)

        fun bindData(cast: Cast) {
            val url = Constains.BASE_POSTER_URL + cast.profilePath
            Glide.with(itemView).load(url).into(imgCaster)
            txtNameCaster.text = cast.name
        }
    }
}