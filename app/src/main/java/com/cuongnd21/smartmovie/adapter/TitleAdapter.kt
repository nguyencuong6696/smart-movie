package com.cuongnd21.smartmovie.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cuongnd21.smartmovie.R
import com.cuongnd21.smartmovie.model.TitleWithResult
import com.cuongnd21.smartmovie.repository.MovieRepository
import com.cuongnd21.smartmovie.ultis.Constains
import com.cuongnd21.smartmovie.ultis.TitleDiffUltis
import javax.inject.Inject

class TitleAdapter @Inject constructor(repository: MovieRepository) :
    RecyclerView.Adapter<TitleAdapter.ViewHolder>() {
    private val titles = arrayListOf<TitleWithResult>()
    var onClickListener: ((Int?) -> Unit) = {}
    var style: Boolean = true
    var movieAdapter = MovieAdapter(repository)

    fun addData(title: List<TitleWithResult>) {
        try {
            val diffUtils =
                DiffUtil.calculateDiff(TitleDiffUltis(oldData = titles, newData = title))
            diffUtils.dispatchUpdatesTo(this)
            titles.clear()
            titles.addAll(title)
        } catch (ex: Exception) {
            titles.clear()
            titles.addAll(title)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_title, parent, false)
        return ViewHolder(view, onClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.movieAdapter = movieAdapter
        holder.style = style
        holder.position = position
        holder.bindData(titles[position])
    }

    override fun getItemCount(): Int = titles.size


    class ViewHolder(itemView: View, private var onClick: ((Int?) -> Unit)) :
        RecyclerView.ViewHolder(itemView) {

        private val txtTitle: TextView = itemView.findViewById(R.id.txtTitle)
        private val rvListMovie: RecyclerView = itemView.findViewById(R.id.rvListMovie)
        private val btnSeeAll: Button = itemView.findViewById(R.id.btnSeeAll)
        lateinit var movieAdapter: MovieAdapter
        var style: Boolean? = null
        var position: Int? = null
        private val viewPool = RecyclerView.RecycledViewPool()

        init {
            btnSeeAll.setOnClickListener {
                onClick.invoke(position)
            }
        }

        fun bindData(title: TitleWithResult) {
            txtTitle.text = title.title
            movieAdapter.addData(title.results)
            rvListMovie.apply {
                style?.let {
                    layoutManager = if (!it) {
                        GridLayoutManager(
                            context,
                            Constains.GRID_SPAN,
                            GridLayoutManager.VERTICAL,
                            false
                        )
                    } else {
                        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    }
                    movieAdapter.style = it
                }
                hasFixedSize()
                adapter = movieAdapter
                setRecycledViewPool(viewPool)
            }
        }
    }
}