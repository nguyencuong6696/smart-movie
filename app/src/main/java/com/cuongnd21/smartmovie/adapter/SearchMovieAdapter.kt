package com.cuongnd21.smartmovie.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cuongnd21.smartmovie.R
import com.cuongnd21.smartmovie.model.Result
import com.cuongnd21.smartmovie.repository.MovieRepository
import com.cuongnd21.smartmovie.ultis.Constains
import com.cuongnd21.smartmovie.ultis.MovieDiffUltis
import kotlinx.coroutines.*
import javax.inject.Inject

class SearchMovieAdapter @Inject constructor(private val repository: MovieRepository) :
    RecyclerView.Adapter<SearchMovieAdapter.ViewHolder>() {
    private val results = arrayListOf<Result>()
    var onItemClick: ((Int?) -> Unit) = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search, parent, false)
        return ViewHolder(view, onItemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.movieId = results[position].id
        holder.repository = repository
        holder.bindData(results[position])
    }

    override fun getItemCount(): Int = results.size

    fun addData(result: List<Result>) {
        try {
            val diffUtils =
                DiffUtil.calculateDiff(MovieDiffUltis(oldData = results, newData = result))
            diffUtils.dispatchUpdatesTo(this)
            results.clear()
            results.addAll(result)
        } catch (ex: Exception) {
            results.clear()
            results.addAll(result)
        }
    }

    class ViewHolder(itemView: View, private val onclick: ((Int?) -> Unit)) :
        RecyclerView.ViewHolder(itemView) {
        private val txtName: TextView = itemView.findViewById(R.id.txtName)
        private val txtGenres: TextView = itemView.findViewById(R.id.txtGenres)
        private val imgMovie: ImageView = itemView.findViewById(R.id.imgMovie)
        private val star1: ImageView = itemView.findViewById(R.id.star1)
        private val star2: ImageView = itemView.findViewById(R.id.star2)
        private val star3: ImageView = itemView.findViewById(R.id.star3)
        private val star4: ImageView = itemView.findViewById(R.id.star4)
        private val star5: ImageView = itemView.findViewById(R.id.star5)

        var movieId: Int? = null
        private var job: Job? = null
        var repository: MovieRepository? = null

        init {
            itemView.setOnClickListener {
                onclick.invoke(movieId)
            }
        }

        fun bindData(result: Result) {
            job?.cancel()
            job = CoroutineScope(Dispatchers.Main).launch {
                val movie = movieId?.let { repository?.getMovieDetail(it) }
                var genres = ""
                txtName.text = result.title
                val url = Constains.BASE_POSTER_URL + result.posterPath
                Glide.with(itemView).load(url).into(imgMovie)
                if (movie != null) {
                    movie.genres.forEach {
                        genres += "${it.name} |"
                    }
                    txtGenres.text = genres.dropLast(2)
                    showStar(movie.voteAverage.toInt())
                }
                cancel()
            }
        }

        private fun showStar(average: Int) {
            when (average) {
                in 1..2 -> {
                    star1.setImageResource(R.drawable.ic_baseline_star_24)
                }
                in 2..4 -> {
                    star1.setImageResource(R.drawable.ic_baseline_star_24)
                    star2.setImageResource(R.drawable.ic_baseline_star_24)
                }
                in 4..6 -> {
                    star1.setImageResource(R.drawable.ic_baseline_star_24)
                    star2.setImageResource(R.drawable.ic_baseline_star_24)
                    star3.setImageResource(R.drawable.ic_baseline_star_24)
                }
                in 6..9 -> {
                    star1.setImageResource(R.drawable.ic_baseline_star_24)
                    star2.setImageResource(R.drawable.ic_baseline_star_24)
                    star3.setImageResource(R.drawable.ic_baseline_star_24)
                    star4.setImageResource(R.drawable.ic_baseline_star_24)
                }
                10 -> {
                    star1.setImageResource(R.drawable.ic_baseline_star_24)
                    star2.setImageResource(R.drawable.ic_baseline_star_24)
                    star3.setImageResource(R.drawable.ic_baseline_star_24)
                    star4.setImageResource(R.drawable.ic_baseline_star_24)
                    star5.setImageResource(R.drawable.ic_baseline_star_24)
                }
            }
        }
    }
}