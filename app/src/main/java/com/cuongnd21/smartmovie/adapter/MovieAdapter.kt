package com.cuongnd21.smartmovie.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cuongnd21.smartmovie.R
import com.cuongnd21.smartmovie.model.FavoriteMovie
import com.cuongnd21.smartmovie.model.Result
import com.cuongnd21.smartmovie.repository.MovieRepository
import com.cuongnd21.smartmovie.ultis.Constains
import com.cuongnd21.smartmovie.ultis.Convert.convertTime
import com.cuongnd21.smartmovie.ultis.Convert.fomatMovieTimeRelease
import com.cuongnd21.smartmovie.ultis.MovieDiffUltis
import kotlinx.coroutines.*
import javax.inject.Inject

class MovieAdapter @Inject constructor(private val repository: MovieRepository) :
    RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
    val results = arrayListOf<Result>()
    var onClickItem: ((Int?) -> Unit) = {}
    var onStarClick: ((Int?) -> Unit) = {}
    var style = false
    val favorite = mutableListOf<FavoriteMovie>()

    enum class StarAdapterPayloads {
        ISSTAR_STATUS
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (!style) {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_movie_grid, parent, false)
            ViewHolder.ItemGridView(view, onClickItem, onStarClick)
        } else {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_movie_linear, parent, false)
            ViewHolder.ItemLinearView(view, onClickItem, onStarClick)
        }
    }

    fun addData(result: List<Result>) {
        try {
            val diffUtils =
                DiffUtil.calculateDiff(MovieDiffUltis(oldData = results, newData = result))
            diffUtils.dispatchUpdatesTo(this)
            results.clear()
            results.addAll(result)
        } catch (ex: Exception) {
            results.clear()
            results.addAll(result)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val result = results[position]
        when (holder) {
            is ViewHolder.ItemGridView -> holder.bindData(result)
            is ViewHolder.ItemLinearView -> holder.bindData(result)
        }
        holder.repository = repository
        holder.movieId = result.id
        holder.position = position
        holder.favorite.addAll(favorite)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isNotEmpty()) {
            payloads.forEach { action ->
                when (action) {
                    StarAdapterPayloads.ISSTAR_STATUS -> {
                        val updateMovie = results[position]
                        when (holder) {
                            is ViewHolder.ItemGridView -> holder.updateStarStatus(updateMovie.isStar)
                            is ViewHolder.ItemLinearView -> holder.updateStarStatus(updateMovie.isStar)
                        }
                    }
                }
            }
        } else {
            super.onBindViewHolder(holder, position, payloads)
        }
    }

    fun onUpdateStarPayloads(result: Result, type: StarAdapterPayloads) {
        val updatePosition = results.indexOfFirst {
            it.id == result.id
        }
        if (updatePosition == -1) return

        when (type) {
            StarAdapterPayloads.ISSTAR_STATUS -> {
                notifyItemChanged(updatePosition, StarAdapterPayloads.ISSTAR_STATUS)
            }
        }
    }

    override fun getItemCount(): Int = results.size

    sealed class ViewHolder(
        itemview: View
    ) : RecyclerView.ViewHolder(itemview) {
        var movieId: Int? = null
        var repository: MovieRepository? = null
        var job: Job? = null
        var position: Int? = null
        val favorite = mutableListOf<FavoriteMovie>()

        class ItemGridView(
            itemView: View,
            private val itemClick: ((Int?) -> Unit),
            private val starClick: ((Int?) -> Unit)
        ) : ViewHolder(itemView) {
            private val txtNameMovie: TextView? = itemView.findViewById(R.id.txtNameMovie)
            private val txtDuration: TextView? = itemView.findViewById(R.id.txtDuration)
            private val imgMovie: ImageView? = itemView.findViewById(R.id.imgMovie)
            private val imgStar: ImageView? = itemView.findViewById(R.id.imgStar)

            init {
                itemView.setOnClickListener {
                    itemClick.invoke(movieId)
                }
                imgStar?.setOnClickListener {
                    starClick.invoke(position)
                }
            }

            fun bindData(result: Result) {
                job?.cancel()
                job = CoroutineScope(Dispatchers.Main).launch {
                    val movie = movieId?.let { repository?.getMovieDetail(it) }
                    favorite.forEach {
                        if (it.id == result.id) {
                            updateStarStatus(it.isStar)
                        }
                    }
                    movie?.let {
                        txtDuration?.text = movie.runtime.convertTime()
                    }
                    txtNameMovie?.text = result.title
                    val url = Constains.BASE_POSTER_URL + result.posterPath
                    if (imgMovie != null) {
                        Glide.with(itemView).load(url).into(imgMovie)
                    }
                    cancel()
                }
            }

            fun updateStarStatus(isStar: Boolean) {
                if (isStar) {
                    imgStar?.setImageResource(R.drawable.ic_baseline_star_24)
                } else {
                    imgStar?.setImageResource(R.drawable.ic_baseline_star_gray_24)
                }
            }
        }

        class ItemLinearView(
            itemView: View,
            private val itemClick: ((Int?) -> Unit),
            private val starClick: ((Int?) -> Unit)
        ) : ViewHolder(itemView) {
            private val txtNameMovie: TextView? = itemView.findViewById(R.id.txtNameMovie)
            private val txtDecription: TextView? = itemView.findViewById(R.id.txtDecription)
            private val txtDuration: TextView? = itemView.findViewById(R.id.txtDuration)
            private val imgMovie: ImageView? = itemView.findViewById(R.id.imgMovie)
            private val imgStar: ImageView? = itemView.findViewById(R.id.imgStar)

            init {
                itemView.setOnClickListener {
                    itemClick.invoke(movieId)
                }
                imgStar?.setOnClickListener {
                    starClick.invoke(position)
                }
            }

            fun bindData(result: Result) {
                job?.cancel()
                job = CoroutineScope(Dispatchers.Main).launch {
                    val movie = movieId?.let { repository?.getMovieDetail(it) }
                    favorite.forEach {
                        if (it.id == result.id) {
                            result.isStar = it.isStar
                            updateStarStatus(it.isStar)
                        }
                    }
                    movie?.let {
                        txtDecription?.text = movie.overview
                        txtDuration?.text = movie.runtime.convertTime()
                        txtNameMovie?.text =
                            "${movie.title} (${movie.releaseDate.fomatMovieTimeRelease()})"
                    }
                    val url = Constains.BASE_POSTER_URL + result.posterPath
                    if (imgMovie != null) {
                        Glide.with(itemView).load(url).into(imgMovie)
                    }
                    cancel()
                }
            }

            fun updateStarStatus(isStar: Boolean) {
                if (isStar) {
                    imgStar?.setImageResource(R.drawable.ic_baseline_star_24)
                } else {
                    imgStar?.setImageResource(R.drawable.ic_baseline_star_gray_24)
                }
            }
        }

    }
}