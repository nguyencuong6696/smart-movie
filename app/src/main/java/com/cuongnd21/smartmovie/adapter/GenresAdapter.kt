package com.cuongnd21.smartmovie.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cuongnd21.smartmovie.R
import com.cuongnd21.smartmovie.model.Genre
import com.cuongnd21.smartmovie.repository.GenresRepository
import com.cuongnd21.smartmovie.ultis.Constains
import com.cuongnd21.smartmovie.ultis.GenreDiffUltis
import kotlinx.coroutines.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GenresAdapter @Inject constructor(var repository: GenresRepository) :
    RecyclerView.Adapter<GenresAdapter.ViewHolder>() {
    private val genres = arrayListOf<Genre>()
    var onItemClick: ((Int?) -> Unit) = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_genres, parent, false)
        return ViewHolder(view, onItemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val genre = genres[position]
        holder.repository = repository
        holder.bindData(genre)
    }

    override fun getItemCount(): Int = genres.size

    fun addData(genre: List<Genre>) {
        try {
            val diffUtils =
                DiffUtil.calculateDiff(GenreDiffUltis(oldData = genres, newData = genre))
            diffUtils.dispatchUpdatesTo(this)
            genres.clear()
            genres.addAll(genre)
        } catch (ex: Exception) {
            genres.clear()
            genres.addAll(genre)
        }
    }

    class ViewHolder(itemView: View, private val onclick: ((Int?) -> Unit)) :
        RecyclerView.ViewHolder(itemView) {
        private val txtGenres: TextView = itemView.findViewById(R.id.txtGenres)
        private val imgGenres: ImageView = itemView.findViewById(R.id.imgGenres)
        private var genresId: Int? = null
        var repository: GenresRepository? = null
        private var job: Job? = null

        init {
            itemView.setOnClickListener {
                onclick.invoke(genresId)
            }
        }

        fun bindData(genre: Genre) {
            genresId = genre.id
            job?.cancel()
            job = CoroutineScope(Dispatchers.Main).launch {
                val movies = repository?.getAllMovieByGenre(genre.id)?.results
                if (movies != null) {
                    val url = Constains.BASE_POSTER_URL + movies[0].posterPath
                    Glide.with(itemView).load(url).into(imgGenres)
                }
                txtGenres.text = genre.name
                cancel()
            }
        }
    }
}