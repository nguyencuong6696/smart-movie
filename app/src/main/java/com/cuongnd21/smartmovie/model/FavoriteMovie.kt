package com.cuongnd21.smartmovie.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.cuongnd21.smartmovie.ultis.Constains

@Entity(tableName = Constains.TABLE_FAVORITE)
data class FavoriteMovie(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    var isStar: Boolean = false
)
