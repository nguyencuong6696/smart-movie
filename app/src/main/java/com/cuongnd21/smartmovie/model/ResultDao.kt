package com.cuongnd21.smartmovie.model

import androidx.room.*
import com.cuongnd21.smartmovie.ultis.Constains

@Dao
interface ResultDao {
    @Query("SELECT * FROM ${Constains.TABLE_FAVORITE}")
    fun getData(): List<FavoriteMovie>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg favoriteMovie: FavoriteMovie)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateData(favoriteMovie: FavoriteMovie)

    @Query("SELECT * FROM ${Constains.TABLE_FAVORITE} WHERE isStar = :isStar")
    fun getDataIsStar(isStar: Boolean): List<FavoriteMovie>
}