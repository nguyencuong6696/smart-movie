package com.cuongnd21.smartmovie.model

data class TitleWithResult(
    val title: String,
    val results: List<Result>
)
