package com.cuongnd21.smartmovie.model

import com.google.gson.annotations.SerializedName

data class SimilarMovie(
    val page: Int,
    val results: List<Result>,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("total_results")
    val totalResults: Int
)