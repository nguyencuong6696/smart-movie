package com.cuongnd21.smartmovie.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.widget.Toolbar
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.cuongnd21.smartmovie.R
import com.cuongnd21.smartmovie.viewmodel.MainActivityViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val mainActivityViewModel: MainActivityViewModel by viewModels()
    private lateinit var navController: NavController
    private lateinit var bottomNavigation: BottomNavigationView
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        initAction()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val menuBottomNavigation = menu?.findItem(R.id.navSearch)
        mainActivityViewModel.isShowSearch.observe(this) {
            menuBottomNavigation?.isVisible = it
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_style -> {
                mainActivityViewModel.changeMode()
                mainActivityViewModel.style.observe(this) {
                    if (it) {
                        item.setIcon(R.drawable.ic_baseline_view_list_24)
                    } else {
                        item.setIcon(R.drawable.ic_baseline_view_agenda_24)
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initView() {
        bottomNavigation = findViewById(R.id.bottomNavigation)
        toolbar = findViewById(R.id.toolBar)
        setSupportActionBar(toolbar)
        val navHost =
            supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
        navController = navHost.navController

        val appBarConfiguration = AppBarConfiguration(navController.graph)
        toolbar.setupWithNavController(navController, appBarConfiguration)
        bottomNavigation.setupWithNavController(navController)
    }

    private fun initAction() {
        bottomNavigation.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.navSearch -> {
                    navController.popBackStack(R.id.homeFragment, false)
                    navController.navigate(R.id.action_homeFragment_to_searchFragment)
                }
                R.id.navGenres -> {
                    navController.popBackStack(R.id.homeFragment, false)
                    navController.navigate(R.id.action_homeFragment_to_genresFragment)
                }
                R.id.navDiscover -> {
                    navController.popBackStack(R.id.homeFragment, false)
                }
            }
            return@setOnItemSelectedListener true
        }
    }

    override fun onDestroy() {
        finish()
        super.onDestroy()
    }
}