package com.cuongnd21.smartmovie.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.cuongnd21.smartmovie.R
import com.cuongnd21.smartmovie.adapter.MovieAdapter
import com.cuongnd21.smartmovie.base.BaseFragments
import com.cuongnd21.smartmovie.model.FavoriteMovie
import com.cuongnd21.smartmovie.model.Result
import com.cuongnd21.smartmovie.ultis.Constains
import com.cuongnd21.smartmovie.ultis.ResultLoadData
import com.cuongnd21.smartmovie.ultis.ToastDiffUltis
import com.cuongnd21.smartmovie.viewmodel.FavoriteViewModel
import com.cuongnd21.smartmovie.viewmodel.MainActivityViewModel
import com.cuongnd21.smartmovie.viewmodel.TopRatedViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import javax.inject.Inject

@AndroidEntryPoint
class TopRatedFragment : BaseFragments(),
    NestedScrollView.OnScrollChangeListener {
    companion object {
        fun newInstance() = TopRatedFragment()
        private const val TAG = "TopRatedFragment"
    }

    private var currentPage = Constains.FIRST_PAGE_INDEX
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()
    private val topRatedViewModel: TopRatedViewModel by viewModels()
    private val favoriteViewModel: FavoriteViewModel by viewModels()

    @Inject
    lateinit var movieAdapter: MovieAdapter
    private val result = arrayListOf<Result>()

    private var job: Job? = null
    private var jobRefresh: Job? = null
    private var isDataLoaded = false
    private var isDataLoadMore = false

    private var rvListTopRated: RecyclerView? = null
    private var nestedScrollView: NestedScrollView? = null
    private var pbLoading: ProgressBar? = null
    private var swipeRefresh: SwipeRefreshLayout? = null

    override fun getFragmentId(): Int = R.layout.fragment_top_rated

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (topRatedViewModel.stateRv != null) {
            rvListTopRated?.layoutManager?.onRestoreInstanceState(topRatedViewModel.stateRv)
            topRatedViewModel.stateRv = null
        }
    }

    override fun initView(view: View) {
        super.initView(view)
        rvListTopRated = view.findViewById(R.id.rvListTopRated)
        nestedScrollView = view.findViewById(R.id.nestedScrollView)
        pbLoading = view.findViewById(R.id.pbLoading)
        swipeRefresh = view.findViewById(R.id.swipeRefresh)
    }

    override fun initData() {
        super.initData()
        favoriteViewModel.getData()
    }

    override fun initAction() {
        super.initAction()
        movieAdapter.apply {
            onClickItem = { movieId ->
                val action = HomeFragmentDirections.actionHomeFragmentToMovieDetailFragment()
                navigationToMovieDetailFragment(action, movieId)
            }
            onStarClick = { position ->
                if (position != null) {
                    val result = movieAdapter.results[position]
                    result.isStar = !result.isStar
                    favoriteViewModel.insertData(FavoriteMovie(result.id, result.isStar))
                    movieAdapter.onUpdateStarPayloads(
                        result,
                        MovieAdapter.StarAdapterPayloads.ISSTAR_STATUS
                    )
                }
            }
        }
        nestedScrollView?.setOnScrollChangeListener(this)
        swipeRefresh?.setOnRefreshListener {
            jobRefresh?.cancel()
            jobRefresh = CoroutineScope(Dispatchers.IO).launch {
                delay(Constains.DELAY_TIME)
                currentPage = 1
                result.clear()
                isDataLoaded = false
                topRatedViewModel.getAllTopRated(currentPage)
                cancel()
            }
            context?.let {
                ToastDiffUltis.show(
                    it,
                    getString(R.string.reload_success),
                    Toast.LENGTH_SHORT
                )
            }
            swipeRefresh?.isRefreshing = false
        }
    }

    private fun navigationToMovieDetailFragment(
        action: HomeFragmentDirections.ActionHomeFragmentToMovieDetailFragment,
        movieId: Int?
    ) {
        if (movieId != null) {
            action.movieId = movieId
            findNavController().navigate(action)
        }
    }

    override fun observeLiveData() {
        super.observeLiveData()
        topRatedViewModel.resultLoadData.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> {
                    loadData()
                }
                ResultLoadData.LOAD_FAIL -> {
                    pbLoading?.visibility = View.INVISIBLE
                    rvListTopRated?.scrollToPosition(result.size)
                    Toast.makeText(context, getString(R.string.error_network), Toast.LENGTH_SHORT)
                        .show()
                }
                null -> Log.e(TAG, "#observeLiveData() : resultLoadData has null")
            }
        }
        favoriteViewModel.favorites.observe(this) { favorite ->
            movieAdapter.favorite.addAll(favorite)
        }

        mainActivityViewModel.style.observe(this) {
            if (it != null) {
                movieAdapter.style = it
                rvListTopRated?.apply {
                    layoutManager = if (!it) {
                        GridLayoutManager(
                            context,
                            Constains.GRID_SPAN,
                            GridLayoutManager.VERTICAL,
                            false
                        )
                    } else {
                        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    }
                    adapter = movieAdapter
                }
            }
        }
    }

    private fun loadData() {
        topRatedViewModel.topRated.observe(this) {
            if (!isDataLoaded || isDataLoadMore) {
                isDataLoaded = true
                result.addAll(it)
                movieAdapter.addData(result)
                isDataLoadMore = false
            }

        }
    }

    override fun onScrollChange(
        v: NestedScrollView?,
        scrollX: Int,
        scrollY: Int,
        oldScrollX: Int,
        oldScrollY: Int
    ) {
        if (v != null) {
            if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight) {
                job?.cancel()
                job = CoroutineScope(Dispatchers.Main).launch {
                    delay(Constains.DELAY_TIME)
                    currentPage++
                    isDataLoadMore = true
                    pbLoading?.visibility = View.VISIBLE
                    topRatedViewModel.getAllTopRated(currentPage)
                    cancel()
                }
            }
        }
    }

    override fun onDestroyView() {
        job?.cancel()
        job = null
        jobRefresh?.cancel()
        jobRefresh = null
        topRatedViewModel.stateRv = rvListTopRated?.layoutManager?.onSaveInstanceState()
        super.onDestroyView()
    }
}