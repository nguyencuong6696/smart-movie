package com.cuongnd21.smartmovie.view.fragment

import android.app.AlertDialog
import android.content.DialogInterface
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.viewpager2.widget.ViewPager2
import com.cuongnd21.smartmovie.R
import com.cuongnd21.smartmovie.adapter.FragmentAdapter
import com.cuongnd21.smartmovie.base.BaseFragments
import com.cuongnd21.smartmovie.ultis.ApiResultMode
import com.cuongnd21.smartmovie.ultis.Constains
import com.cuongnd21.smartmovie.ultis.ResultLoadData
import com.cuongnd21.smartmovie.ultis.ToastDiffUltis
import com.cuongnd21.smartmovie.viewmodel.HomeViewModel
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragments() {
    private val homeViewModel: HomeViewModel by activityViewModels()
    private var isLoaded = false
    private var isPopularLoaded = false
    private var isNowPlayingLoaded = false
    private var isUpComingLoaded = false
    private var isTopRatedLoaded = false
    private val fragments = mutableListOf<Fragment>()
    private val tabName = mutableListOf<String>()
    private var fragmentAdapter: FragmentAdapter? = null

    companion object {
        private const val TAG = "HomeFragment"
    }

    private var pbLoading: ProgressBar? = null
    private var tabLayout: TabLayout? = null
    private var viewPager2: ViewPager2? = null

    override fun getFragmentId(): Int = R.layout.fragment_home

    override fun initData() {
        super.initData()
        homeViewModel.getAllData()
    }

    override fun initView(view: View) {
        super.initView(view)
        tabLayout = view.findViewById(R.id.tabLayout)
        viewPager2 = view.findViewById(R.id.viewPager2)
        pbLoading = view.findViewById(R.id.pbLoading)
        pbLoading?.visibility = View.VISIBLE
        fragmentAdapter = FragmentAdapter(this)
        if (isLoaded) {
            tabName.forEach {
                tabLayout?.newTab()?.setText(it)?.let { it1 -> tabLayout?.addTab(it1) }
            }
            fragmentAdapter?.fragments = fragments.toTypedArray()
            viewPager2?.adapter = fragmentAdapter
        }
    }

    override fun initAction() {
        super.initAction()
        tabLayout?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab != null) {
                    viewPager2?.currentItem = tab.position
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })
        viewPager2?.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                tabLayout?.selectTab(tabLayout?.getTabAt(position))
            }
        })
    }

    override fun observeLiveData() {
        super.observeLiveData()
        homeViewModel.resultLoadData.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> {
                    pbLoading?.visibility = View.INVISIBLE
                    loadData()
                }
                ResultLoadData.LOAD_FAIL -> {
                    pbLoading?.visibility = View.INVISIBLE
                    context?.let { context ->
                        ToastDiffUltis.show(context, ApiResultMode.ERROR.value, Toast.LENGTH_SHORT)
                    }
                    showAlert(Constains.FAILED_ALERT, Constains.DESCRIPTION_ALERT)
                }
                null -> Log.e(TAG, "#observeLiveData() : resultLoadData has null")
            }
        }

        homeViewModel.changeFragment.observe(this) { position ->
            viewPager2?.setCurrentItem(position + 1, true)
            tabLayout?.selectTab(tabLayout?.getTabAt(position + 1))
        }
    }

    private fun loadData() {
        homeViewModel.resultLoadPopular.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> loadDataPopular()
                ResultLoadData.LOAD_FAIL -> context?.let { context ->
                    ToastDiffUltis.show(
                        context,
                        resources.getString(R.string.fragment_load, Constains.POPULAR),
                        Toast.LENGTH_SHORT
                    )
                }
                null -> Log.e(TAG, "#loadData() : resultLoadPopular has null")
            }
        }
        homeViewModel.resultLoadTopRated.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> loadDataTopRated()
                ResultLoadData.LOAD_FAIL -> context?.let { context ->
                    ToastDiffUltis.show(
                        context,
                        resources.getString(R.string.fragment_load, Constains.TOPRATED),
                        Toast.LENGTH_SHORT
                    )
                }
                null -> Log.e(TAG, "#loadData() : resultLoadTopRated has null")
            }
        }
        homeViewModel.resultLoadUpcoming.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> loadDataUpComing()
                ResultLoadData.LOAD_FAIL -> context?.let { context ->
                    ToastDiffUltis.show(
                        context,
                        resources.getString(R.string.fragment_load, Constains.UPCOMING),
                        Toast.LENGTH_SHORT
                    )
                }
                null -> Log.e(TAG, "#loadData() : resultLoadUpcoming has null")
            }
        }
        homeViewModel.resultLoadNowPlaying.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> loadDataNowPlaying()
                ResultLoadData.LOAD_FAIL -> context?.let { context ->
                    ToastDiffUltis.show(
                        context,
                        resources.getString(R.string.fragment_load, Constains.NOWPLAYING),
                        Toast.LENGTH_SHORT
                    )
                }
                null -> Log.e(TAG, "#loadData() : resultLoadNowPlaying has null")
            }
        }
    }

    private fun loadDataNowPlaying() {
        homeViewModel.nowPlayingWithTitle.observe(this) {
            if (!isNowPlayingLoaded) {
                isNowPlayingLoaded = true
                addNewFragment(NowPlayingFragment.newInstance(), "Now Playing")
            }
        }
    }

    private fun loadDataUpComing() {
        homeViewModel.upComingWithTitle.observe(this) {
            if (!isUpComingLoaded) {
                isUpComingLoaded = true
                addNewFragment(UpComingFragment.newInstance(), "Up Coming")
            }
        }
    }

    private fun loadDataTopRated() {
        homeViewModel.topRatedWithTitle.observe(this) {
            if (!isTopRatedLoaded) {
                isTopRatedLoaded = true
                addNewFragment(TopRatedFragment.newInstance(), Constains.TOPRATED)
            }
        }
    }

    private fun loadDataPopular() {
        homeViewModel.popularWithTitle.observe(this) {
            if (!isPopularLoaded) {
                isPopularLoaded = true
                addNewFragment(PopularFragment.newInstance(), Constains.POPULAR)
            }
        }
    }

    override fun onPause() {
        homeViewModel.changeFragment((viewPager2?.currentItem ?: 1) - 1)
        super.onPause()
    }

    private fun showAlert(title: String, description: String) {
        val alert = AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(description)
            .setPositiveButton("Reload", DialogInterface.OnClickListener { dialog, _ ->
                homeViewModel.getAllData()
                dialog.dismiss()
            }).create()
        alert.show()
    }

    private fun addNewFragment(fragment: Fragment, title: String) {
        if (!isLoaded) {
            isLoaded = true
            addNewFragment(MoviesFragment.newInstance(), "Movies")
        }
        fragments.add(fragment)
        tabName.add(title)
        tabLayout?.newTab()?.setText(title)?.let { tabLayout?.addTab(it) }
        fragmentAdapter?.fragments = fragments.toTypedArray()
        viewPager2?.adapter = fragmentAdapter
    }
}