package com.cuongnd21.smartmovie.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cuongnd21.smartmovie.R
import com.cuongnd21.smartmovie.adapter.MovieAdapter
import com.cuongnd21.smartmovie.adapter.TitleAdapter
import com.cuongnd21.smartmovie.base.BaseFragments
import com.cuongnd21.smartmovie.model.FavoriteMovie
import com.cuongnd21.smartmovie.model.TitleWithResult
import com.cuongnd21.smartmovie.ultis.Constains
import com.cuongnd21.smartmovie.ultis.ResultLoadData
import com.cuongnd21.smartmovie.ultis.ToastDiffUltis
import com.cuongnd21.smartmovie.viewmodel.FavoriteViewModel
import com.cuongnd21.smartmovie.viewmodel.HomeViewModel
import com.cuongnd21.smartmovie.viewmodel.MainActivityViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MoviesFragment : BaseFragments() {
    private val homeViewModel: HomeViewModel by activityViewModels()
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()
    private val favoriteViewModel: FavoriteViewModel by viewModels()
    private val titles = arrayListOf<TitleWithResult>()
    private var popularIsLoaded = false
    private var topratedIsLoaded = false
    private var upComingIsLoaded = false
    private var nowPlayingIsLoaded = false

    private var rvListMovie: RecyclerView? = null

    @Inject
    lateinit var titleAdapter: TitleAdapter

    companion object {
        fun newInstance() = MoviesFragment()
        private const val TAG = "MoviesFragment"
    }

    override fun getFragmentId(): Int = R.layout.fragment_movies

    override fun initView(view: View) {
        super.initView(view)
        rvListMovie = view.findViewById(R.id.rvListMovie)
        rvListMovie?.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            hasFixedSize()
            adapter = titleAdapter
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (homeViewModel.stateRv != null) {
            rvListMovie?.layoutManager?.onRestoreInstanceState(homeViewModel.stateRv)
            homeViewModel.stateRv = null
        }
    }

    override fun initData() {
        super.initData()
        favoriteViewModel.getData()
    }

    override fun initAction() {
        super.initAction()
        titleAdapter.movieAdapter.apply {
            onClickItem = { movieId ->
                val action = HomeFragmentDirections.actionHomeFragmentToMovieDetailFragment()
                navigationToMovieDetailFragment(action, movieId)
            }
            onStarClick = { position ->
                if (position != null) {
                    val result = titleAdapter.movieAdapter.results[position]
                    result.isStar = !result.isStar
                    favoriteViewModel.insertData(FavoriteMovie(result.id, result.isStar))
                    titleAdapter.movieAdapter.onUpdateStarPayloads(
                        result,
                        MovieAdapter.StarAdapterPayloads.ISSTAR_STATUS
                    )
                }
            }
        }
        titleAdapter.onClickListener = { position ->
            if (position != null) {
                homeViewModel.changeFragment(position)
            }
        }
    }

    private fun navigationToMovieDetailFragment(
        action: HomeFragmentDirections.ActionHomeFragmentToMovieDetailFragment,
        movieId: Int?
    ) {
        if (movieId != null) {
            action.movieId = movieId
            findNavController().navigate(action)
        }
    }

    override fun observeLiveData() {
        super.observeLiveData()
        homeViewModel.resultLoadPopular.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> loadDataPopular()
                ResultLoadData.LOAD_FAIL -> context?.let { context ->
                    ToastDiffUltis.show(
                        context,
                        resources.getString(R.string.fragment_load, Constains.POPULAR),
                        Toast.LENGTH_SHORT
                    )
                }
                null -> Log.e(TAG, "#loadData() : resultLoadPopular has null")
            }
        }
        homeViewModel.resultLoadTopRated.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> loadDataTopRated()
                ResultLoadData.LOAD_FAIL -> context?.let { context ->
                    ToastDiffUltis.show(
                        context,
                        resources.getString(R.string.fragment_load, Constains.TOPRATED),
                        Toast.LENGTH_SHORT
                    )
                }
                null -> Log.e(TAG, "#loadData() : resultLoadTopRated has null")
            }
        }
        homeViewModel.resultLoadUpcoming.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> loadDataUpComing()
                ResultLoadData.LOAD_FAIL -> context?.let { context ->
                    ToastDiffUltis.show(
                        context,
                        resources.getString(R.string.fragment_load, Constains.UPCOMING),
                        Toast.LENGTH_SHORT
                    )
                }
                null -> Log.e(TAG, "#loadData() : resultLoadUpcoming has null")
            }
        }
        homeViewModel.resultLoadNowPlaying.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> loadDataNowPlaying()
                ResultLoadData.LOAD_FAIL -> context?.let { context ->
                    ToastDiffUltis.show(
                        context,
                        resources.getString(R.string.fragment_load, Constains.NOWPLAYING),
                        Toast.LENGTH_SHORT
                    )
                }
                null -> Log.e(TAG, "#loadData() : resultLoadNowPlaying has null")
            }
        }
        mainActivityViewModel.style.observe(this) {
            if (it != null) {
                titleAdapter.style = it
                rvListMovie?.adapter = titleAdapter
            }
        }
        favoriteViewModel.favorites.observe(this) { favorite ->
            titleAdapter.movieAdapter.favorite.addAll(favorite)
        }
    }

    private fun loadDataNowPlaying() {
        homeViewModel.nowPlayingWithTitle.observe(this) {
            if (!nowPlayingIsLoaded) {
                nowPlayingIsLoaded = !nowPlayingIsLoaded
                addData(it!!)

            }
        }
    }

    private fun loadDataUpComing() {
        homeViewModel.upComingWithTitle.observe(this) {
            if (!upComingIsLoaded) {
                upComingIsLoaded = !upComingIsLoaded
                addData(it!!)
            }
        }
    }

    private fun loadDataTopRated() {
        homeViewModel.topRatedWithTitle.observe(this) {
            if (!topratedIsLoaded) {
                topratedIsLoaded = !topratedIsLoaded
                addData(it!!)
            }
        }
    }

    private fun loadDataPopular() {
        homeViewModel.popularWithTitle.observe(this) {
            if (!popularIsLoaded) {
                popularIsLoaded = !popularIsLoaded
                addData(it!!)
            }
        }
    }

    private fun addData(title: TitleWithResult) {
        titles.add(title)
        titleAdapter.addData(titles)
    }

    override fun onDestroyView() {
        homeViewModel.stateRv = rvListMovie?.layoutManager?.onSaveInstanceState()
        super.onDestroyView()
    }
}