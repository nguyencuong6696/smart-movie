package com.cuongnd21.smartmovie.view.fragment

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cuongnd21.smartmovie.R
import com.cuongnd21.smartmovie.adapter.GenresAdapter
import com.cuongnd21.smartmovie.base.BaseFragments
import com.cuongnd21.smartmovie.model.Genre
import com.cuongnd21.smartmovie.ultis.ResultLoadData
import com.cuongnd21.smartmovie.ultis.ToastDiffUltis
import com.cuongnd21.smartmovie.viewmodel.GenresViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class GenresFragment : BaseFragments() {
    private val genresViewModel: GenresViewModel by viewModels()

    companion object {
        private const val TAG = "GenresFragment"
    }

    @Inject
    lateinit var genresAdapter: GenresAdapter
    private val genres = mutableListOf<Genre>()
    private var rvGenres: RecyclerView? = null
    private var isLoad = false

    override fun initView(view: View) {
        super.initView(view)
        rvGenres = view.findViewById(R.id.rvGenres)
        rvGenres?.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            hasFixedSize()
            adapter = genresAdapter
        }
    }

    override fun getFragmentId(): Int = R.layout.fragment_genres

    override fun initData() {
        super.initData()
        genresViewModel.getListGenre()
    }

    override fun initAction() {
        super.initAction()
        genresAdapter.onItemClick = { genreId ->
            val action = getActionNavigateToMoviesGenreFragment(genreId)
            getNavigateToMoviesGenreFragment(action)
        }
    }

    private fun getActionNavigateToMoviesGenreFragment(genreId: Int?): GenresFragmentDirections.ActionGenresFragmentToMoviesGenre? {
        return if (genreId != null) {
            var name = ""
            genres.forEach { genre ->
                if (genreId == genre.id) {
                    name = genre.name
                }
            }
            GenresFragmentDirections.actionGenresFragmentToMoviesGenre(genreId, name)
        } else null
    }

    private fun getNavigateToMoviesGenreFragment(action: GenresFragmentDirections.ActionGenresFragmentToMoviesGenre?) {
        if (action != null) {
            findNavController().navigate(action)
        }
    }

    override fun observeLiveData() {
        super.observeLiveData()
        genresViewModel.resultLoadData.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> {
                    loadData()
                }
                ResultLoadData.LOAD_FAIL -> {
                    context?.let { context ->
                        ToastDiffUltis.show(
                            context,
                            getString(R.string.error_network),
                            Toast.LENGTH_SHORT
                        )
                    }
                }
                null -> Log.e(TAG, "#observeLiveData() : resultLoadData has null")
            }
        }
    }

    private fun loadData() {
        genresViewModel.genres.observe(this) {
            if (!isLoad) {
                isLoad = true
                genres.addAll(it.genres)
                genresAdapter.addData(genres)
            }
        }
    }
}