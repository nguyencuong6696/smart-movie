package com.cuongnd21.smartmovie.view.fragment

import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cuongnd21.smartmovie.R
import com.cuongnd21.smartmovie.adapter.CrewAdapter
import com.cuongnd21.smartmovie.adapter.SearchMovieAdapter
import com.cuongnd21.smartmovie.base.BaseFragments
import com.cuongnd21.smartmovie.model.Cast
import com.cuongnd21.smartmovie.ultis.Constains
import com.cuongnd21.smartmovie.ultis.Convert.convertTime
import com.cuongnd21.smartmovie.ultis.Convert.fomatMovieTimeRelease
import com.cuongnd21.smartmovie.ultis.ResultLoadData
import com.cuongnd21.smartmovie.ultis.ToastDiffUltis
import com.cuongnd21.smartmovie.viewmodel.MovieDetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import com.cuongnd21.smartmovie.ultis.MySpannable


@AndroidEntryPoint
class MovieDetailFragment : BaseFragments() {
    private val viewModel: MovieDetailViewModel by viewModels()
    private val args: MovieDetailFragmentArgs by navArgs()
    private val crewAdapter = CrewAdapter()

    @Inject
    lateinit var similarAdapter: SearchMovieAdapter
    private val casts = mutableListOf<Cast>()

    private var rvCast: RecyclerView? = null
    private var rvSimilar: RecyclerView? = null

    private var txtNameMovie: TextView? = null
    private var txtDecription: TextView? = null
    private var txtTimeRelease: TextView? = null
    private var txtImdb: TextView? = null
    private var txtLanguage: TextView? = null
    private var txtType: TextView? = null

    private var imgMovie: ImageView? = null
    private var star1: ImageView? = null
    private var star2: ImageView? = null
    private var star3: ImageView? = null
    private var star4: ImageView? = null
    private var star5: ImageView? = null

    companion object {
        private const val TAG = "MovieDetailFragment"
    }

    override fun initView(view: View) {
        super.initView(view)
        txtNameMovie = view.findViewById(R.id.txtNameMovie)
        txtDecription = view.findViewById(R.id.txtDecription)
        txtTimeRelease = view.findViewById(R.id.txtTimeRelease)
        txtImdb = view.findViewById(R.id.txtImdb)
        txtLanguage = view.findViewById(R.id.txtLanguage)
        txtType = view.findViewById(R.id.txtType)

        imgMovie = view.findViewById(R.id.imgMovie)
        star1 = view.findViewById(R.id.star1)
        star2 = view.findViewById(R.id.star2)
        star3 = view.findViewById(R.id.star3)
        star4 = view.findViewById(R.id.star4)
        star5 = view.findViewById(R.id.star5)

        rvCast = view.findViewById(R.id.rvCast)
        rvSimilar = view.findViewById(R.id.rvSimilar)
        rvCast?.apply {
            layoutManager =
                GridLayoutManager(context, Constains.GRID_SPAN, GridLayoutManager.HORIZONTAL, false)
            hasFixedSize()
            adapter = crewAdapter
        }
        rvSimilar?.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            hasFixedSize()
            adapter = similarAdapter
        }
    }

    override fun getFragmentId(): Int {
        return R.layout.fragment_movie_detail
    }

    override fun initData() {
        super.initData()
        val movieId = args.movieId
        viewModel.getDetailMovie(movieId)
        viewModel.getTeamMovie(movieId)
        viewModel.getSimilarMovie(movieId, Constains.FIRST_PAGE_INDEX)
    }

    override fun observeLiveData() {
        super.observeLiveData()
        viewModel.resultLoadDetailMovie.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> loadDetailMovie()
                ResultLoadData.LOAD_FAIL -> context?.let { context ->
                    ToastDiffUltis.show(
                        context,
                        getString(R.string.error_network),
                        Toast.LENGTH_SHORT
                    )
                }
                null -> Log.e(TAG, "#observeLiveData() : resultLoadDetailMovie has null")
            }
        }
        viewModel.resultLoadTeamMovie.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> loadTeam()
                ResultLoadData.LOAD_FAIL -> context?.let { context ->
                    ToastDiffUltis.show(
                        context,
                        getString(R.string.error_network),
                        Toast.LENGTH_SHORT
                    )
                }
                null -> Log.e(TAG, "#observeLiveData() : resultLoadTeamMovie has null")
            }
        }
        viewModel.resultLoadSimilarMovie.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> loadSimilarMovie()
                ResultLoadData.LOAD_FAIL -> context?.let { context ->
                    ToastDiffUltis.show(
                        context,
                        getString(R.string.error_network),
                        Toast.LENGTH_SHORT
                    )
                }
                null -> Log.e(TAG, "#observeLiveData() : resultLoadSimilarMovie has null")
            }
        }
    }

    private fun loadSimilarMovie() {
        viewModel.similarMovie.observe(this) {
            similarAdapter.addData(it)
        }
    }

    private fun loadTeam() {
        viewModel.team.observe(this) {
            casts.addAll(it.cast)
            crewAdapter.addData(casts)
        }
    }

    private fun loadDetailMovie() {
        viewModel.movie.observe(this) {
            var genres = ""
            txtNameMovie?.text = it.title
            txtDecription?.text = it.overview
            txtDecription?.let { txtDecription -> makeTextViewResizable(txtDecription, 3, "See more", true) }
            txtTimeRelease?.text = resources.getString(
                R.string.time_duration,
                it.releaseDate.fomatMovieTimeRelease(),
                if (it.productionCountries.isNotEmpty()) {
                    it.productionCountries[0].name
                } else {
                    ""
                },
                it.runtime.convertTime()
            )
            txtImdb?.text = resources.getString(R.string.average, it.voteAverage.toString())
            if (it.spokenLanguages.isNotEmpty()) {
                txtLanguage?.text =
                    resources.getString(R.string.language, it.spokenLanguages[0].english_name)
            }
            it.genres.forEach { genre ->
                genres += "${genre.name} |"
            }
            txtType?.text = genres.dropLast(2)
            showStar(it.voteAverage.toInt())
            val url = Constains.BASE_POSTER_URL + it.posterPath
            context?.let { context -> imgMovie?.let { imgMovie -> Glide.with(context).load(url).into(imgMovie) } }
        }
    }

    private fun showStar(average: Int) {
        when (average) {
            in 1..2 -> {
                star1?.setImageResource(R.drawable.ic_baseline_star_24)
            }
            in 2..4 -> {
                star1?.setImageResource(R.drawable.ic_baseline_star_24)
                star2?.setImageResource(R.drawable.ic_baseline_star_24)
            }
            in 4..6 -> {
                star1?.setImageResource(R.drawable.ic_baseline_star_24)
                star2?.setImageResource(R.drawable.ic_baseline_star_24)
                star3?.setImageResource(R.drawable.ic_baseline_star_24)
            }
            in 6..9 -> {
                star1?.setImageResource(R.drawable.ic_baseline_star_24)
                star2?.setImageResource(R.drawable.ic_baseline_star_24)
                star3?.setImageResource(R.drawable.ic_baseline_star_24)
                star4?.setImageResource(R.drawable.ic_baseline_star_24)
            }
            10 -> {
                star1?.setImageResource(R.drawable.ic_baseline_star_24)
                star2?.setImageResource(R.drawable.ic_baseline_star_24)
                star3?.setImageResource(R.drawable.ic_baseline_star_24)
                star4?.setImageResource(R.drawable.ic_baseline_star_24)
                star5?.setImageResource(R.drawable.ic_baseline_star_24)
            }
        }
    }

    fun makeTextViewResizable(tv: TextView, maxLine: Int, expandText: String, viewMore: Boolean) {
        if (tv.tag == null) {
            tv.tag = tv.text
        }
        val vto = tv.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                val obs = tv.viewTreeObserver
                obs.removeGlobalOnLayoutListener(this)
                if (maxLine == 0) {
                    val lineEndIndex = tv.layout.getLineEnd(0)
                    val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1)
                        .toString() + " " + expandText
                    tv.text = text
                    tv.movementMethod = LinkMovementMethod.getInstance()
                    tv.setText(
                        addClickablePartTextViewResizable(
                            Html.fromHtml(tv.text.toString()), tv, expandText, viewMore
                        ), TextView.BufferType.SPANNABLE
                    )
                } else if (maxLine > 0 && tv.lineCount >= maxLine) {
                    val lineEndIndex = tv.layout.getLineEnd(maxLine - 1)
                    val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1)
                        .toString() + " " + expandText
                    tv.text = text
                    tv.movementMethod = LinkMovementMethod.getInstance()
                    tv.setText(
                        addClickablePartTextViewResizable(
                            Html.fromHtml(tv.text.toString()), tv, expandText, viewMore
                        ), TextView.BufferType.SPANNABLE
                    )
                } else {
                    val lineEndIndex = tv.layout.getLineEnd(tv.layout.lineCount - 1)
                    val text = tv.text.subSequence(0, lineEndIndex).toString() + " " + expandText
                    tv.text = text
                    tv.movementMethod = LinkMovementMethod.getInstance()
                    tv.setText(
                        addClickablePartTextViewResizable(
                            Html.fromHtml(tv.text.toString()), tv, expandText, viewMore
                        ), TextView.BufferType.SPANNABLE
                    )
                }
            }
        })
    }

    private fun addClickablePartTextViewResizable(
        strSpanned: Spanned, tv: TextView,
        spanableText: String, viewMore: Boolean
    ): SpannableStringBuilder {
        val str = strSpanned.toString()
        val ssb = SpannableStringBuilder(strSpanned)
        if (str.contains(spanableText)) {
            ssb.setSpan(object : MySpannable(false) {
                override fun onClick(p0: View) {
                    if (viewMore) {
                        tv.layoutParams = tv.layoutParams
                        tv.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
                        tv.invalidate()
                        makeTextViewResizable(tv, -1, "See Less", false)
                    } else {
                        tv.layoutParams = tv.layoutParams
                        tv.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
                        tv.invalidate()
                        makeTextViewResizable(tv, 3, ".. See More", true)
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length, 0)
        }
        return ssb
    }
}