package com.cuongnd21.smartmovie.view.fragment

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cuongnd21.smartmovie.R
import com.cuongnd21.smartmovie.adapter.SearchMovieAdapter
import com.cuongnd21.smartmovie.base.BaseFragments
import com.cuongnd21.smartmovie.model.Result
import com.cuongnd21.smartmovie.ultis.ResultLoadData
import com.cuongnd21.smartmovie.ultis.ToastDiffUltis
import com.cuongnd21.smartmovie.viewmodel.GenresViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MoviesGenre : BaseFragments() {
    private lateinit var rvMovies: RecyclerView

    companion object {
        private const val TAG = "MoviesGenre"
    }

    @Inject
    lateinit var searchMovieAdapter: SearchMovieAdapter
    private val genresViewModel: GenresViewModel by viewModels()
    private val args: MoviesGenreArgs by navArgs()
    private val movies = mutableListOf<Result>()
    private var isLoaded = false

    override fun getFragmentId(): Int = R.layout.fragment_movies_genre

    override fun initView(view: View) {
        super.initView(view)
        val nameGenre = args.name
        activity?.title = resources.getString(R.string.title_genre, nameGenre)
        rvMovies = view.findViewById(R.id.rvMovies)
        rvMovies.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            hasFixedSize()
            adapter = searchMovieAdapter
        }
    }

    override fun initData() {
        super.initData()
        val genreId = args.genreId
        genresViewModel.getAllMoviesGenres(genreId)
    }

    override fun initAction() {
        super.initAction()
        searchMovieAdapter.onItemClick = { movieId ->
            val action = MoviesGenreDirections.actionMoviesGenreToMovieDetailFragment()
            navigationToMovieDetailFragment(action, movieId)
        }
    }

    private fun navigationToMovieDetailFragment(
        action: MoviesGenreDirections.ActionMoviesGenreToMovieDetailFragment,
        movieId: Int?
    ) {
        if (movieId != null) {
            action.movieId = movieId
            findNavController().navigate(action)
        }
    }

    override fun observeLiveData() {
        super.observeLiveData()
        genresViewModel.resultLoadData.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> {
                    loadData()
                }
                ResultLoadData.LOAD_FAIL -> {
                    context?.let { context ->
                        ToastDiffUltis.show(
                            context,
                            getString(R.string.error_network),
                            Toast.LENGTH_SHORT
                        )
                    }
                }
                null -> Log.e(TAG, "#observeLiveData() : resultLoadData has null")
            }
        }

    }

    private fun loadData() {
        genresViewModel.movies.observe(this) {
            if (!isLoaded) {
                isLoaded = true
                movies.addAll(it)
                searchMovieAdapter.addData(movies)
            }
        }
    }
}