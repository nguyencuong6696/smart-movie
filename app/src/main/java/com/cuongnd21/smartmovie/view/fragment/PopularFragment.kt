package com.cuongnd21.smartmovie.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.cuongnd21.smartmovie.R
import com.cuongnd21.smartmovie.adapter.MovieAdapter
import com.cuongnd21.smartmovie.base.BaseFragments
import com.cuongnd21.smartmovie.model.FavoriteMovie
import com.cuongnd21.smartmovie.model.Result
import com.cuongnd21.smartmovie.ultis.Constains
import com.cuongnd21.smartmovie.ultis.ResultLoadData
import com.cuongnd21.smartmovie.ultis.ToastDiffUltis
import com.cuongnd21.smartmovie.viewmodel.FavoriteViewModel
import com.cuongnd21.smartmovie.viewmodel.MainActivityViewModel
import com.cuongnd21.smartmovie.viewmodel.PopularViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import javax.inject.Inject

@AndroidEntryPoint
class PopularFragment : BaseFragments(), NestedScrollView.OnScrollChangeListener {
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()
    private val popularViewModel: PopularViewModel by activityViewModels()
    private val favoriteViewModel: FavoriteViewModel by viewModels()

    @Inject
    lateinit var movieAdapter: MovieAdapter
    private val result = arrayListOf<Result>()
    private var currentPage = Constains.FIRST_PAGE_INDEX

    private var isDataLoaded = false
    private var isDataLoadMore = false

    private var job: Job? = null
    private var jobRefresh: Job? = null

    private var rvListPopular: RecyclerView? = null
    private var nestedScrollView: NestedScrollView? = null
    private var pbLoading: ProgressBar? = null
    private var swipeRefresh: SwipeRefreshLayout? = null

    companion object {
        fun newInstance() = PopularFragment()
        private const val TAG = "PopularFragment"
    }

    override fun getFragmentId(): Int = R.layout.fragment_popular

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (popularViewModel.stateRv != null) {
            rvListPopular?.layoutManager?.onRestoreInstanceState(popularViewModel.stateRv)
            popularViewModel.stateRv = null
        }
    }

    override fun initView(view: View) {
        super.initView(view)
        rvListPopular = view.findViewById(R.id.rvListPopular)
        nestedScrollView = view.findViewById(R.id.nestedScrollView)
        pbLoading = view.findViewById(R.id.pbLoading)
        swipeRefresh = view.findViewById(R.id.swipeRefresh)
    }

    override fun initData() {
        super.initData()
        favoriteViewModel.getData()
    }

    override fun initAction() {
        super.initAction()
        movieAdapter.apply {
            onClickItem = { movieId ->
                val action = HomeFragmentDirections.actionHomeFragmentToMovieDetailFragment()
                navigationToMovieDetailFragment(action, movieId)
            }
            onStarClick = { position ->
                if (position != null) {
                    val result = movieAdapter.results[position]
                    result.isStar = !result.isStar
                    favoriteViewModel.insertData(FavoriteMovie(result.id, result.isStar))
                    movieAdapter.onUpdateStarPayloads(
                        result,
                        MovieAdapter.StarAdapterPayloads.ISSTAR_STATUS
                    )
                }
            }
        }
        nestedScrollView?.setOnScrollChangeListener(this)
        swipeRefresh?.setOnRefreshListener {
            jobRefresh?.cancel()
            jobRefresh = CoroutineScope(Dispatchers.IO).launch {
                delay(Constains.DELAY_TIME)
                currentPage = 1
                result.clear()
                isDataLoaded = false
                popularViewModel.getAllPopular(currentPage)
                cancel()
            }
            Toast.makeText(context, getString(R.string.reload_success), Toast.LENGTH_SHORT).show()
            swipeRefresh?.isRefreshing = false
        }
    }

    private fun navigationToMovieDetailFragment(
        action: HomeFragmentDirections.ActionHomeFragmentToMovieDetailFragment,
        movieId: Int?
    ) {
        if (movieId != null) {
            action.movieId = movieId
            findNavController().navigate(action)
        }
    }

    override fun observeLiveData() {
        super.observeLiveData()
        popularViewModel.resultLoadData.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> {
                    loadData()
                }
                ResultLoadData.LOAD_FAIL -> {
                    pbLoading?.visibility = View.INVISIBLE
                    rvListPopular?.scrollToPosition(result.size)
                    context?.let { context ->
                        ToastDiffUltis.show(
                            context,
                            getString(R.string.reload_success),
                            Toast.LENGTH_SHORT
                        )
                    }
                }
                null -> Log.e(TAG, "#observeLiveData() : resultLoadData has null")
            }
        }
        favoriteViewModel.favorites.observe(this) { favorite ->
            movieAdapter.favorite.addAll(favorite)
        }
        mainActivityViewModel.style.observe(this) {
            movieAdapter.style = it
            rvListPopular?.apply {
                layoutManager = if (!it) {
                    GridLayoutManager(
                        context,
                        Constains.GRID_SPAN,
                        GridLayoutManager.VERTICAL,
                        false
                    )
                } else {
                    LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                }
                movieAdapter.addData(result)
                adapter = movieAdapter
            }
        }
    }

    private fun loadData() {
        popularViewModel.popular.observe(this) {
            if (!isDataLoaded || isDataLoadMore) {
                isDataLoaded = true
                result.addAll(it)
                movieAdapter.addData(result)
                isDataLoadMore = false
            }
        }
    }

    override fun onScrollChange(
        v: NestedScrollView?,
        scrollX: Int,
        scrollY: Int,
        oldScrollX: Int,
        oldScrollY: Int
    ) {
        if (v != null) {
            if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight) {
                job?.cancel()
                job = CoroutineScope(Dispatchers.Main).launch {
                    delay(Constains.DELAY_TIME)
                    currentPage++
                    isDataLoadMore = true
                    pbLoading?.visibility = View.VISIBLE
                    popularViewModel.getAllPopular(currentPage)
                    cancel()
                }
            }
        }
    }

    override fun onDestroyView() {
        job?.cancel()
        job = null
        jobRefresh?.cancel()
        jobRefresh = null
        popularViewModel.stateRv = rvListPopular?.layoutManager?.onSaveInstanceState()
        super.onDestroyView()
    }
}