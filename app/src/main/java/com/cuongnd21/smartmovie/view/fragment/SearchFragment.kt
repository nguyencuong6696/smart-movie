package com.cuongnd21.smartmovie.view.fragment

import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cuongnd21.smartmovie.R
import com.cuongnd21.smartmovie.adapter.SearchMovieAdapter
import com.cuongnd21.smartmovie.base.BaseFragments
import com.cuongnd21.smartmovie.model.Result
import com.cuongnd21.smartmovie.ultis.ResultLoadData
import com.cuongnd21.smartmovie.ultis.ToastDiffUltis
import com.cuongnd21.smartmovie.viewmodel.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import javax.inject.Inject

@AndroidEntryPoint
class SearchFragment : BaseFragments(), SearchView.OnQueryTextListener {
    private val searchViewModel: SearchViewModel by viewModels()

    @Inject
    lateinit var searchMovieAdapter: SearchMovieAdapter
    private val results = mutableListOf<Result>()
    private var job: Job? = null

    private lateinit var rvMovies: RecyclerView
    private lateinit var searchBar: SearchView
    private lateinit var btnCancel: Button

    companion object {
        private const val TAG = "SearchFragment"
    }

    override fun initView(view: View) {
        super.initView(view)
        rvMovies = view.findViewById(R.id.rvMovies)
        searchBar = view.findViewById(R.id.search_bar)
        btnCancel = view.findViewById(R.id.btnCancel)
        rvMovies.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            hasFixedSize()
            adapter = searchMovieAdapter
        }
    }

    override fun getFragmentId(): Int = R.layout.fragment_search

    override fun initAction() {
        super.initAction()
        searchBar.setOnQueryTextListener(this)
        searchMovieAdapter.onItemClick = { movieId ->
            val action = SearchFragmentDirections.actionSearchFragmentToMovieDetailFragment()
            navigationToMovieDetailFragment(action, movieId)
        }
        btnCancel.setOnClickListener {
            results.clear()
            searchMovieAdapter.addData(results)
        }
    }

    private fun navigationToMovieDetailFragment(
        action: SearchFragmentDirections.ActionSearchFragmentToMovieDetailFragment,
        movieId: Int?
    ) {
        if (movieId != null) {
            action.movieId = movieId
            findNavController().navigate(action)
        }
    }

    override fun observeLiveData() {
        super.observeLiveData()
        searchViewModel.resultLoadData.observe(this) {
            when (it) {
                ResultLoadData.LOAD_SUCCESS -> {
                    loadData()
                }
                ResultLoadData.LOAD_FAIL -> {
                    context?.let { context ->
                        ToastDiffUltis.show(
                            context,
                            getString(R.string.error_network),
                            Toast.LENGTH_SHORT
                        )
                    }
                }
                null -> Log.e(TAG, "#observeLiveData() : resultLoadData has null")
            }
        }
    }

    private fun loadData() {
        searchViewModel.result.observe(this) {
            results.clear()
            results.addAll(it.results)
            searchMovieAdapter.addData(results)
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (newText?.isNotEmpty() == true) {
            job?.cancel()
            job = CoroutineScope(Dispatchers.Main).launch {
                searchViewModel.getSearch(newText)
                delay(1000)
                cancel()
            }
        } else {
            results.clear()
            searchMovieAdapter.addData(results)
        }
        return true
    }
}