package com.cuongnd21.smartmovie.ultis

import retrofit2.Response

object RestApi {
    fun <T> getData(response: Response<T>): T? {
        return if (response.isSuccessful) {
            response.body()
        } else {
            null
        }
    }
}