package com.cuongnd21.smartmovie.ultis

enum class ResultLoadData {
    LOAD_SUCCESS, LOAD_FAIL
}