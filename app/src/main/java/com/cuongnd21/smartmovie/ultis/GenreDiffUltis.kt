package com.cuongnd21.smartmovie.ultis

import androidx.recyclerview.widget.DiffUtil
import com.cuongnd21.smartmovie.model.Genre

class GenreDiffUltis(private val oldData: List<Genre>, private val newData: List<Genre>) :
    DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldData.size

    override fun getNewListSize(): Int = newData.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldGenre = oldData.getOrNull(oldItemPosition)
        val newGenre = newData.getOrNull(newItemPosition)

        if (oldGenre != null && newGenre != null) {
            return oldGenre.id == newGenre.id
        }
        return false
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldGenre = oldData.getOrNull(oldItemPosition)
        val newGenre = newData.getOrNull(newItemPosition)

        if (oldGenre != null && newGenre != null) {
            return oldGenre.id == newGenre.id
        }
        return false
    }
}