package com.cuongnd21.smartmovie.ultis

import androidx.recyclerview.widget.DiffUtil
import com.cuongnd21.smartmovie.model.Cast

class CrewDiffUltis(private val oldData: List<Cast>, private val newData: List<Cast>) :
    DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldData.size

    override fun getNewListSize(): Int = newData.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldCast = oldData.getOrNull(oldItemPosition)
        val newCast = newData.getOrNull(newItemPosition)

        if (oldCast != null && newCast != null) {
            return oldCast.originalName == newCast.originalName
        }
        return false
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldCast = oldData.getOrNull(oldItemPosition)
        val newCast = newData.getOrNull(newItemPosition)

        if (oldCast != null && newCast != null) {
            return oldCast.originalName == newCast.originalName
        }
        return false
    }
}