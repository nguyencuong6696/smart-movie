package com.cuongnd21.smartmovie.ultis

import androidx.recyclerview.widget.DiffUtil
import com.cuongnd21.smartmovie.model.Result

class MovieDiffUltis(private val oldData: List<Result>, private val newData: List<Result>) :
    DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldData.size

    override fun getNewListSize(): Int = newData.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldResult = oldData.getOrNull(oldItemPosition)
        val newResult = newData.getOrNull(newItemPosition)

        if (oldResult != null && newResult != null) {
            return oldResult.id == newResult.id
        }
        return false
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldResult = oldData.getOrNull(oldItemPosition)
        val newResult = newData.getOrNull(newItemPosition)

        if (oldResult != null && newResult != null) {
            return oldResult.isStar == newResult.isStar
        }
        return false
    }
}