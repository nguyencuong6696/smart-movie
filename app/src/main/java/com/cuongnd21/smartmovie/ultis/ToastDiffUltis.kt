package com.cuongnd21.smartmovie.ultis

import android.content.Context
import android.widget.Toast

object ToastDiffUltis {
    private var toast: Toast? = null
    fun show(context: Context, message: String, time: Int) {
        toast?.cancel()
        toast = Toast.makeText(context, message, time)
        toast?.show()
    }
}