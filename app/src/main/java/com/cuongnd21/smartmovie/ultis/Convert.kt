package com.cuongnd21.smartmovie.ultis

import android.annotation.SuppressLint
import java.text.DateFormat
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*

object Convert {
    @SuppressLint("SimpleDateFormat")
    fun String.fomatMovieTimeRelease(): String {
        val runTime = this
        if (runTime.isEmpty()) {
            return ""
        } else {
            val sdf = SimpleDateFormat("yyyy-M-dd")
            val date: Date? = sdf.parse(runTime)
            val calendar = Calendar.getInstance()
            return if (date != null) {
                calendar.time = date
                val day = calendar.get(Calendar.DAY_OF_MONTH)
                val month = DateFormatSymbols(Locale.ENGLISH).months[calendar.get(Calendar.MONTH)]
                val year = calendar.get(Calendar.YEAR)
                "$month ${day}, $year"
            } else {
                ""
            }
        }
    }

    fun Int.convertTime(): String {
        val runtime = this
        val hour = runtime / 60
        val minute = runtime % 60
        return if (runtime > 60) {
            "${hour}h, ${minute}mins"
        } else {
            "${runtime}mins"
        }
    }
}