package com.cuongnd21.smartmovie.ultis

import androidx.recyclerview.widget.DiffUtil
import com.cuongnd21.smartmovie.model.TitleWithResult

class TitleDiffUltis(
    private val oldData: List<TitleWithResult>,
    private val newData: List<TitleWithResult>
) :
    DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldData.size

    override fun getNewListSize(): Int = newData.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldResult = oldData.getOrNull(oldItemPosition)
        val newResult = newData.getOrNull(newItemPosition)

        if (oldResult != null && newResult != null) {
            return oldResult.results == newResult.results
        }
        return false
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldResult = oldData.getOrNull(oldItemPosition)
        val newResult = newData.getOrNull(newItemPosition)

        if (oldResult != null && newResult != null) {
            return oldResult.results == newResult.results
        }
        return false
    }
}