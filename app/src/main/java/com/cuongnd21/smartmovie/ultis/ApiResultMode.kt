package com.cuongnd21.smartmovie.ultis

enum class ApiResultMode(val value: String) {
    SUCCESS("success"),
    ERROR("error")
}