package com.cuongnd21.smartmovie.ultis

object Constains {
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val BASE_SEARCH_MOVIE_URL = "movie?"
    const val GET_PAGE = "&page="
    const val API_KEY = "api_key="
    const val POPULAR_URL = "popular?"
    const val TOP_RATE_URL = "top_rated?"
    const val UPCOMING_URL = "upcoming?"
    const val NOW_PLAYING_URL = "now_playing?"
    const val QUERY_URL = "&query="
    const val CREDIT_URL = "credits?"
    const val SIMILAR_URL = "similar?"
    const val BASE_POSTER_URL = "https://image.tmdb.org/t/p/w342/"
    const val GENRES_URL = "list?"
    const val FAILED_ALERT = "Load data failed"
    const val DESCRIPTION_ALERT = "Can't get data from server, Please try again later."
    const val DURATION_LOAD_API = 1000L
    const val DELAY_TIME = 500L
    const val FIRST_PAGE_INDEX = 1
    const val GRID_SPAN = 2
    const val TABLE_FAVORITE = "favorite"
    const val POPULAR = "Popular"
    const val NOWPLAYING = "Now Playing"
    const val UPCOMING = "Up Coming"
    const val TOPRATED = "Top Rated"
    const val LOAD_DATA_SUCCESS = "Load data success"
    const val LOAD_DATA_FAIL = "Load data fail"
}