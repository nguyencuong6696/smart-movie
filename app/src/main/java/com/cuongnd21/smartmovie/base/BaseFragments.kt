package com.cuongnd21.smartmovie.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class BaseFragments : Fragment() {

    abstract fun getFragmentId(): Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getFragmentId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLiveData()
        initData()
        initView(view)
        initAction()

    }

    open fun initView(view: View) = Unit

    open fun initData() = Unit

    open fun initAction() = Unit

    open fun observeLiveData() = Unit
}