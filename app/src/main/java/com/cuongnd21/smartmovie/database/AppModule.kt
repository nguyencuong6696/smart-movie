package com.cuongnd21.smartmovie.database

import android.content.Context
import androidx.room.Room
import com.cuongnd21.smartmovie.model.ResultDao
import com.cuongnd21.smartmovie.repository.FavoriteRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideMovieDatabase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(
        context,
        ResultDatabase::class.java,
        "My database"
    ).build()

    @Singleton
    @Provides
    fun provideMovieDao(db: ResultDatabase): ResultDao = db.resultDao()

    @Singleton
    @Provides
    fun providesFavorite(db: ResultDao): FavoriteRepository = FavoriteRepository(db)
}