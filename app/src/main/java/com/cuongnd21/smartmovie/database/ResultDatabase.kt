package com.cuongnd21.smartmovie.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.cuongnd21.smartmovie.model.FavoriteMovie
import com.cuongnd21.smartmovie.model.ResultDao

@Database(entities = [FavoriteMovie::class], version = 1, exportSchema = false)
abstract class ResultDatabase : RoomDatabase() {
    abstract fun resultDao(): ResultDao
}