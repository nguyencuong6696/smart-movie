package com.cuongnd21.smartmovie.api

import com.cuongnd21.smartmovie.BuildConfig.API_KEY
import com.cuongnd21.smartmovie.model.*
import com.cuongnd21.smartmovie.ultis.Constains
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TheMovieDB {

    @GET("movie/" + Constains.POPULAR_URL + Constains.API_KEY + API_KEY + Constains.GET_PAGE)
    fun getListPopular(@Query("page") page: Int): Call<TitlePage>

    @GET("movie/" + Constains.TOP_RATE_URL + Constains.API_KEY + API_KEY + Constains.GET_PAGE)
    fun getLisTopRated(@Query("page") page: Int): Call<TitlePage>

    @GET("movie/" + Constains.UPCOMING_URL + Constains.API_KEY + API_KEY + Constains.GET_PAGE)
    fun getListUpComing(@Query("page") page: Int): Call<TitlePage>

    @GET("movie/" + Constains.NOW_PLAYING_URL + Constains.API_KEY + API_KEY + Constains.GET_PAGE)
    fun getListNowPlaying(@Query("page") page: Int): Call<TitlePage>

    @GET("movie/{movie_id}/" + Constains.SIMILAR_URL + Constains.API_KEY + API_KEY + Constains.GET_PAGE)
    fun getSimilarMovie(
        @Path("movie_id") movieId: Int,
        @Query("page") page: Int
    ): Call<SimilarMovie>

    @GET("movie/{movie_id}?" + Constains.API_KEY + API_KEY)
    fun getMovieDetail(@Path("movie_id") movie_id: Int): Call<Movie>

    @GET("genre/movie/" + Constains.GENRES_URL + Constains.API_KEY + API_KEY)
    fun getAllGenres(): Call<GenreList>

    @GET("movie/{movie_id}/" + Constains.CREDIT_URL + Constains.API_KEY + API_KEY)
    fun getAllCrewInMovie(@Path("movie_id") movieId: Int): Call<Team>

    @GET("search/" + Constains.BASE_SEARCH_MOVIE_URL + Constains.API_KEY + API_KEY + Constains.QUERY_URL)
    fun getSearch(@Query("query") query: String?): Call<Search>

    @GET("discover/" + Constains.BASE_SEARCH_MOVIE_URL + Constains.API_KEY + API_KEY)
    fun getALlMovieByGenres(@Query("with_genres") genreId: Int): Call<SimilarMovie>
}