package com.cuongnd21.smartmovie

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ApplicationContext : Application()
