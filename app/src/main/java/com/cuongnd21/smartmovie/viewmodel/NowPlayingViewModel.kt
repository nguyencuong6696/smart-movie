package com.cuongnd21.smartmovie.viewmodel

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cuongnd21.smartmovie.model.Result
import com.cuongnd21.smartmovie.repository.MovieRepository
import com.cuongnd21.smartmovie.ultis.Constains
import com.cuongnd21.smartmovie.ultis.ResultLoadData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NowPlayingViewModel @Inject constructor(private val repository: MovieRepository) :
    ViewModel() {
    var stateRv: Parcelable? = null
    private var job: Job? = null

    private var _nowPlaying = MutableLiveData<List<Result>>()
    val nowPlaying: LiveData<List<Result>>
        get() = _nowPlaying

    private var _resultLoadData = MutableLiveData<ResultLoadData>()
    val resultLoadData: LiveData<ResultLoadData>
        get() = _resultLoadData

    init {
        getAllNowPlaying(Constains.FIRST_PAGE_INDEX)
    }

    fun getAllNowPlaying(page: Int) {
        job?.cancel()
        job = viewModelScope.launch(Dispatchers.IO) {
            if (repository.getAllNowPlaying(page) != null) {
                _nowPlaying.postValue(repository.getAllNowPlaying(page)!!.results)
                _resultLoadData.postValue(ResultLoadData.LOAD_SUCCESS)
            } else {
                _resultLoadData.postValue(ResultLoadData.LOAD_FAIL)
            }
        }
    }
}