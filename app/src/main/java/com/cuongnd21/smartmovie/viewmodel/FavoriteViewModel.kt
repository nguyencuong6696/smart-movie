package com.cuongnd21.smartmovie.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cuongnd21.smartmovie.model.FavoriteMovie
import com.cuongnd21.smartmovie.repository.FavoriteRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(private val repository: FavoriteRepository) :
    ViewModel() {

    private var _favorites = MutableLiveData<List<FavoriteMovie>>()
    val favorites: LiveData<List<FavoriteMovie>>
        get() = _favorites

    fun getData() {
        viewModelScope.launch(Dispatchers.IO) {
            _favorites.postValue(repository.getData())
        }
    }

    fun insertData(favoriteMovie: FavoriteMovie) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.insertData(favoriteMovie)
            _favorites.postValue(repository.getData())
        }
    }
}