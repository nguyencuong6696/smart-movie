package com.cuongnd21.smartmovie.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityViewModel : ViewModel() {
    private var mode: Boolean = false
    private var _style = MutableLiveData(mode)
    val style: LiveData<Boolean>
        get() = _style


    private var _isShowSearch = MutableLiveData<Boolean>()
    val isShowSearch: LiveData<Boolean>
        get() = _isShowSearch

    fun changeMode() {
        mode = !mode
        _style.postValue(mode)
    }

}