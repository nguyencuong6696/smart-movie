package com.cuongnd21.smartmovie.viewmodel

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cuongnd21.smartmovie.model.TitleWithResult
import com.cuongnd21.smartmovie.repository.MovieRepository
import com.cuongnd21.smartmovie.ultis.Constains
import com.cuongnd21.smartmovie.ultis.ResultLoadData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val repository: MovieRepository) : ViewModel() {
    private var popularIsLoaded = false
    private var topratedIsLoaded = false
    private var upComingIsLoaded = false
    private var nowPlayingIsLoaded = false

    var stateRv: Parcelable? = null

    private var job: Job? = null

    private var _popularWithTitle = MutableLiveData<TitleWithResult?>()
    val popularWithTitle: LiveData<TitleWithResult?>
        get() = _popularWithTitle

    private var _nowPlayingWithTitle = MutableLiveData<TitleWithResult?>()
    val nowPlayingWithTitle: LiveData<TitleWithResult?>
        get() = _nowPlayingWithTitle

    private var _topRatedWithTitle = MutableLiveData<TitleWithResult?>()
    val topRatedWithTitle: LiveData<TitleWithResult?>
        get() = _topRatedWithTitle

    private var _upComingWithTitle = MutableLiveData<TitleWithResult?>()
    val upComingWithTitle: LiveData<TitleWithResult?>
        get() = _upComingWithTitle

    private var _changeFragment = MutableLiveData<Int>()
    val changeFragment: LiveData<Int>
        get() = _changeFragment

    private var _resultLoadData = MutableLiveData<ResultLoadData>()
    val resultLoadData: LiveData<ResultLoadData>
        get() = _resultLoadData

    private var _resultLoadPopular = MutableLiveData<ResultLoadData>()
    val resultLoadPopular: LiveData<ResultLoadData>
        get() = _resultLoadPopular

    private var _resultLoadNowPlaying = MutableLiveData<ResultLoadData>()
    val resultLoadNowPlaying: LiveData<ResultLoadData>
        get() = _resultLoadNowPlaying

    private var _resultLoadUpComing = MutableLiveData<ResultLoadData>()
    val resultLoadUpcoming: LiveData<ResultLoadData>
        get() = _resultLoadUpComing

    private var _resultLoadTopRated = MutableLiveData<ResultLoadData>()
    val resultLoadTopRated: LiveData<ResultLoadData>
        get() = _resultLoadTopRated

    fun getAllData() {
        job?.cancel()
        job = viewModelScope.launch(Dispatchers.IO) {
            getDataPopular()
            getDataTopRate()
            getDataUpComing()
            getDataNowPlaying()
            launch(Dispatchers.Main) {
                checkApi()
            }
        }
    }

    private suspend fun checkApi() {
        delay(Constains.DURATION_LOAD_API)
        if (!popularIsLoaded && !nowPlayingIsLoaded && !topratedIsLoaded && !upComingIsLoaded
        ) {
            _resultLoadData.postValue(ResultLoadData.LOAD_FAIL)
        } else {
            _resultLoadData.postValue(ResultLoadData.LOAD_SUCCESS)
        }
    }

    private suspend fun getDataPopular() {
        if (repository.getAllPopular(Constains.FIRST_PAGE_INDEX) != null) {
            popularIsLoaded = true
            _popularWithTitle.postValue(repository.getAllPopular(Constains.FIRST_PAGE_INDEX)!!.results.let {
                TitleWithResult(
                    Constains.POPULAR,
                    if (it.size > 4) it.take(4) else it
                )
            })
            _resultLoadPopular.postValue(ResultLoadData.LOAD_SUCCESS)
        } else {
            _resultLoadPopular.postValue(ResultLoadData.LOAD_FAIL)
        }
    }

    private suspend fun getDataNowPlaying() {
        if (repository.getAllNowPlaying(Constains.FIRST_PAGE_INDEX) != null) {
            nowPlayingIsLoaded = true
            _nowPlayingWithTitle.postValue(repository.getAllNowPlaying(Constains.FIRST_PAGE_INDEX)!!.results.let {
                TitleWithResult(
                    Constains.NOWPLAYING,
                    if (it.size > 4) it.take(4) else it
                )
            })
            _resultLoadNowPlaying.postValue(ResultLoadData.LOAD_SUCCESS)
        } else {
            _resultLoadNowPlaying.postValue(ResultLoadData.LOAD_FAIL)
        }
    }

    private suspend fun getDataTopRate() {
        if (repository.getAllTopRated(Constains.FIRST_PAGE_INDEX) != null) {
            topratedIsLoaded = true
            _topRatedWithTitle.postValue(repository.getAllTopRated(Constains.FIRST_PAGE_INDEX)!!.results.let {
                TitleWithResult(
                    Constains.TOPRATED,
                    if (it.size > 4) it.take(4) else it
                )
            })
            _resultLoadTopRated.postValue(ResultLoadData.LOAD_SUCCESS)
        } else {
            _resultLoadTopRated.postValue(ResultLoadData.LOAD_FAIL)
        }
    }

    private suspend fun getDataUpComing() {
        if (repository.getAllUpComing(Constains.FIRST_PAGE_INDEX) != null) {
            upComingIsLoaded = true
            _upComingWithTitle.postValue(repository.getAllUpComing(Constains.FIRST_PAGE_INDEX)!!.results.let {
                TitleWithResult(
                    Constains.UPCOMING,
                    if (it.size > 4) it.take(4) else it
                )
            })
            _resultLoadUpComing.postValue(ResultLoadData.LOAD_SUCCESS)
        } else {
            _resultLoadUpComing.postValue(ResultLoadData.LOAD_FAIL)
        }
    }

    fun changeFragment(position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            _changeFragment.postValue(position)
        }
    }
}
