package com.cuongnd21.smartmovie.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cuongnd21.smartmovie.model.Search
import com.cuongnd21.smartmovie.repository.SearchRepository
import com.cuongnd21.smartmovie.ultis.ResultLoadData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(private val repository: SearchRepository) : ViewModel() {
    private var _result = MutableLiveData<Search>()
    val result: LiveData<Search>
        get() = _result

    private var _resultLoadData = MutableLiveData<ResultLoadData>()
    val resultLoadData: LiveData<ResultLoadData>
        get() = _resultLoadData

    fun getSearch(char: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            if (repository.getSearch(char) != null) {
                _result.postValue(repository.getSearch(char))
                _resultLoadData.postValue(ResultLoadData.LOAD_SUCCESS)
            } else {
                _resultLoadData.postValue(ResultLoadData.LOAD_FAIL)
            }
        }
    }
}