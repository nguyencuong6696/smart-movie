package com.cuongnd21.smartmovie.viewmodel

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cuongnd21.smartmovie.model.GenreList
import com.cuongnd21.smartmovie.model.Result
import com.cuongnd21.smartmovie.repository.GenresRepository
import com.cuongnd21.smartmovie.ultis.ResultLoadData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GenresViewModel @Inject constructor(private val repository: GenresRepository) : ViewModel() {

    var stateRv: Parcelable? = null

    private var _genres = MutableLiveData<GenreList>()
    val genres: LiveData<GenreList>
        get() = _genres

    private var _movies = MutableLiveData<List<Result>>()
    val movies: LiveData<List<Result>>
        get() = _movies

    private var _resultLoadData = MutableLiveData<ResultLoadData>()
    val resultLoadData: LiveData<ResultLoadData>
        get() = _resultLoadData

    fun getListGenre() {
        viewModelScope.launch(Dispatchers.IO) {
            if (repository.getGenre() != null) {
                _genres.postValue(repository.getGenre())
                _resultLoadData.postValue(ResultLoadData.LOAD_SUCCESS)
            } else {
                _resultLoadData.postValue(ResultLoadData.LOAD_FAIL)
            }
        }
    }

    fun getAllMoviesGenres(genresId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            if (repository.getAllMovieByGenre(genresId) != null) {
                _movies.postValue(repository.getAllMovieByGenre(genresId)!!.results)
                _resultLoadData.postValue(ResultLoadData.LOAD_SUCCESS)
            } else {
                _resultLoadData.postValue(ResultLoadData.LOAD_FAIL)
            }
        }
    }
}