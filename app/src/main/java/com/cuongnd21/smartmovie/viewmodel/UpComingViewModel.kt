package com.cuongnd21.smartmovie.viewmodel

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cuongnd21.smartmovie.model.Result
import com.cuongnd21.smartmovie.repository.MovieRepository
import com.cuongnd21.smartmovie.ultis.Constains
import com.cuongnd21.smartmovie.ultis.ResultLoadData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UpComingViewModel @Inject constructor(private val repository: MovieRepository) :
    ViewModel() {
    var stateRv: Parcelable? = null
    private var job: Job? = null

    private var _upComing = MutableLiveData<List<Result>>()
    val upComing: LiveData<List<Result>>
        get() = _upComing

    private var _resultLoadData = MutableLiveData<ResultLoadData>()
    val resultLoadData: LiveData<ResultLoadData>
        get() = _resultLoadData

    init {
        getAllUpComing(Constains.FIRST_PAGE_INDEX)
    }

    fun getAllUpComing(page: Int) {
        job?.cancel()
        job = viewModelScope.launch(Dispatchers.IO) {
            if (repository.getAllUpComing(page) != null) {
                _upComing.postValue(repository.getAllUpComing(page)!!.results)
                _resultLoadData.postValue(ResultLoadData.LOAD_SUCCESS)
            } else {
                _resultLoadData.postValue(ResultLoadData.LOAD_FAIL)
            }
        }
    }
}