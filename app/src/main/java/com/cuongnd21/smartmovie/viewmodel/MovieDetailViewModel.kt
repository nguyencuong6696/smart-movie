package com.cuongnd21.smartmovie.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cuongnd21.smartmovie.model.Movie
import com.cuongnd21.smartmovie.model.Result
import com.cuongnd21.smartmovie.model.Team
import com.cuongnd21.smartmovie.repository.MovieRepository
import com.cuongnd21.smartmovie.ultis.ResultLoadData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(private val repository: MovieRepository) :
    ViewModel() {
    private var _movie = MutableLiveData<Movie>()
    val movie: LiveData<Movie>
        get() = _movie

    private var _team = MutableLiveData<Team>()
    val team: LiveData<Team>
        get() = _team

    private var _similarMovie = MutableLiveData<List<Result>>()
    val similarMovie: LiveData<List<Result>>
        get() = _similarMovie

    private var _resultLoadDetailMovie = MutableLiveData<ResultLoadData>()
    val resultLoadDetailMovie: LiveData<ResultLoadData>
        get() = _resultLoadDetailMovie

    private var _resultLoadTeamMovie = MutableLiveData<ResultLoadData>()
    val resultLoadTeamMovie: LiveData<ResultLoadData>
        get() = _resultLoadTeamMovie

    private var _resultLoadSimilarMovie = MutableLiveData<ResultLoadData>()
    val resultLoadSimilarMovie: LiveData<ResultLoadData>
        get() = _resultLoadSimilarMovie


    fun getDetailMovie(movieId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            if (repository.getMovieDetail(movieId) != null) {
                _movie.postValue(repository.getMovieDetail(movieId))
                _resultLoadDetailMovie.postValue(ResultLoadData.LOAD_SUCCESS)
            } else {
                _resultLoadDetailMovie.postValue(ResultLoadData.LOAD_FAIL)
            }
        }
    }

    fun getTeamMovie(movieId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            if (repository.getAllCast(movieId) != null) {
                _team.postValue(repository.getAllCast(movieId))
                _resultLoadTeamMovie.postValue(ResultLoadData.LOAD_SUCCESS)
            } else {
                _resultLoadTeamMovie.postValue(ResultLoadData.LOAD_FAIL)
            }
        }
    }

    fun getSimilarMovie(movieId: Int, page: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            if (repository.getSimilarMovie(movieId, page) != null) {
                _similarMovie.postValue(repository.getSimilarMovie(movieId, page)!!.results)
                _resultLoadSimilarMovie.postValue(ResultLoadData.LOAD_SUCCESS)
            } else {
                _resultLoadSimilarMovie.postValue(ResultLoadData.LOAD_FAIL)
            }
        }
    }
}