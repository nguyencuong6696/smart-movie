package com.cuongnd21.smartmovie.repository

import com.cuongnd21.smartmovie.model.FavoriteMovie
import com.cuongnd21.smartmovie.model.ResultDao
import javax.inject.Inject

class FavoriteRepository @Inject constructor(private val resultDao: ResultDao) {

    fun getData(): List<FavoriteMovie> = resultDao.getDataIsStar(true)

    fun insertData(vararg favoriteMovie: FavoriteMovie) {
        resultDao.insertAll(*favoriteMovie)
    }
}