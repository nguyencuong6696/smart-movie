package com.cuongnd21.smartmovie.repository

import android.util.Log
import com.cuongnd21.smartmovie.api.*
import com.cuongnd21.smartmovie.model.*
import com.cuongnd21.smartmovie.ultis.Constains
import com.cuongnd21.smartmovie.ultis.RestApi
import kotlinx.coroutines.*
import retrofit2.HttpException
import retrofit2.awaitResponse
import java.io.IOException
import javax.inject.Inject

class MovieRepository @Inject constructor(private val apiService: TheMovieDB) {
    companion object {
        private const val TAG = "MovieRepository"
    }

    suspend fun getAllPopular(page: Int): TitlePage? {
        var popular: TitlePage? = null
        delay(Constains.DELAY_TIME)
        try {
            val response = apiService.getListPopular(page).awaitResponse()
            popular = RestApi.getData(response)
        } catch (ioE: IOException) {
            Log.e(TAG, "#getAllPopular() error $ioE")
        } catch (connectionE: HttpException) {
            Log.e(TAG, "#getAllPopular() error $connectionE")
        }
        return popular
    }

    suspend fun getAllTopRated(page: Int): TitlePage? {
        var topRated: TitlePage? = null
        delay(Constains.DELAY_TIME)
        try {
            val response = apiService.getLisTopRated(page).awaitResponse()
            topRated = RestApi.getData(response)
        } catch (ioE: IOException) {
            Log.e(TAG, "#getAllTopRated() error $ioE")
        } catch (connectionE: HttpException) {
            Log.e(TAG, "#getAllTopRated() error $connectionE")
        }
        return topRated
    }

    suspend fun getAllUpComing(page: Int): TitlePage? {
        var upComing: TitlePage? = null
        delay(Constains.DELAY_TIME)
        try {
            val response = apiService.getListUpComing(page).awaitResponse()
            upComing = RestApi.getData(response)
        } catch (ioE: IOException) {
            Log.e(TAG, "#getAllUpComing() error $ioE")
        } catch (connectionE: HttpException) {
            Log.e(TAG, "#getAllUpComing() error $connectionE")
        }
        return upComing
    }

    suspend fun getAllNowPlaying(page: Int): TitlePage? {
        var nowPlaying: TitlePage? = null
        delay(Constains.DELAY_TIME)
        try {
            val response = apiService.getListNowPlaying(page).awaitResponse()
            nowPlaying = RestApi.getData(response)
        } catch (ioE: IOException) {
            Log.e(TAG, "#getAllNowPlaying() error $ioE")
        } catch (connectionE: HttpException) {
            Log.e(TAG, "#getAllNowPlaying() error $connectionE")
        }
        return nowPlaying
    }

    suspend fun getMovieDetail(movieId: Int): Movie? {
        var movie: Movie? = null
        try {
            val response = apiService.getMovieDetail(movieId).awaitResponse()
            movie = RestApi.getData(response)
        } catch (ioE: IOException) {
            Log.e(TAG, "#getMovieDetail() error $ioE")
        } catch (connectionE: HttpException) {
            Log.e(TAG, "#getMovieDetail() error $connectionE")
        }
        return movie
    }

    suspend fun getAllCast(movieId: Int): Team? {
        var team: Team? = null
        try {
            val response = apiService.getAllCrewInMovie(movieId).awaitResponse()
            team = RestApi.getData(response)
        } catch (ioE: IOException) {
            Log.e(TAG, "#getAllCast() error $ioE")
        } catch (connectionE: HttpException) {
            Log.e(TAG, "#getAllCast() error $connectionE")
        }
        return team
    }

    suspend fun getSimilarMovie(movieId: Int, page: Int): SimilarMovie? {
        var similar: SimilarMovie? = null
        try {
            val response = apiService.getSimilarMovie(movieId, page).awaitResponse()
            similar = RestApi.getData(response)
        } catch (ioE: IOException) {
            Log.e(TAG, "#getSimilarMovie() error $ioE")
        } catch (connectionE: HttpException) {
            Log.e(TAG, "#getSimilarMovie() error $connectionE")
        }
        return similar
    }
}