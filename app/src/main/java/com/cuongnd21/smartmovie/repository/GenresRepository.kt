package com.cuongnd21.smartmovie.repository

import android.util.Log
import com.cuongnd21.smartmovie.api.TheMovieDB
import com.cuongnd21.smartmovie.model.GenreList
import com.cuongnd21.smartmovie.model.SimilarMovie
import com.cuongnd21.smartmovie.ultis.RestApi
import retrofit2.HttpException
import retrofit2.awaitResponse
import java.io.IOException
import javax.inject.Inject

class GenresRepository @Inject constructor(private val apiService: TheMovieDB) {
    companion object {
        private const val TAG = "GenresRepository"
    }

    suspend fun getGenre(): GenreList? {
        var genres: GenreList? = null
        try {
            val response = apiService.getAllGenres().awaitResponse()
            genres = RestApi.getData(response)
        } catch (ioE: IOException) {
            Log.e(TAG, "#getGenre() error $ioE")
        } catch (connectionE: HttpException) {
            Log.e(TAG, "#getGenre() error $connectionE")
        }
        return genres
    }

    suspend fun getAllMovieByGenre(genreId: Int): SimilarMovie? {
        var similarMovie: SimilarMovie? = null
        try {
            val response = apiService.getALlMovieByGenres(genreId).awaitResponse()
            similarMovie = RestApi.getData(response)
        } catch (ioE: IOException) {
            Log.e(TAG, "#getAllMovieByGenre() error $ioE")
        } catch (connectionE: HttpException) {
            Log.e(TAG, "#getAllMovieByGenre() error $connectionE")
        }
        return similarMovie
    }
}