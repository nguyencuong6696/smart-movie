package com.cuongnd21.smartmovie.repository

import android.util.Log
import com.cuongnd21.smartmovie.api.TheMovieDB
import com.cuongnd21.smartmovie.model.Search
import com.cuongnd21.smartmovie.ultis.RestApi
import retrofit2.HttpException
import retrofit2.awaitResponse
import java.io.IOException
import javax.inject.Inject

class SearchRepository @Inject constructor(private val apiService: TheMovieDB) {
    companion object {
        private const val TAG = "SearchRepository"
    }

    suspend fun getSearch(char: String?): Search? {
        var search: Search? = null
        try {
            val response = apiService.getSearch(char).awaitResponse()
            if (char == null) {
                return null
            } else {
                search = RestApi.getData(response)
            }
        } catch (ioE: IOException) {
            Log.e(TAG, "#getSearch() error $ioE")
        } catch (connectionE: HttpException) {
            Log.e(TAG, "#getSearch() error $connectionE")
        }
        return search
    }
}